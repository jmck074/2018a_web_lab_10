-- Answers to Exercise 5 here
DROP TABLE IF EXISTS dbtest_tablethreeex5;

CREATE TABLE IF NOT EXISTS dbtest_tablethreeex5(
  username VARCHAR (32) NOT NULL,
  first_name VARCHAR(20),
  last_name VARCHAR(50),
  email VARCHAR (50),
  PRIMARY KEY (username)
);

INSERT INTO dbtest_tablethreeex5 VALUES
  ('programmer3', 'Pete', 'M', 'pmei050@aucklanduni.ac.nz'),
  ('programmer4', 'Peter', 'Smith', 'psmith@wodehouse.co.uk'),
  ('programmer5', 'Kevin', 'Peterson', 'programmer5@notcricket.com');
  -- ('programmer5', 'Johnny','5','is@alive.com');

SELECT * FROM dbtest_tablethreeex5;