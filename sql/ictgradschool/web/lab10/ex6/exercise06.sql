-- Answers to Exercise 6 here
DROP TABLE IF EXISTS dbtest_tableSix;

CREATE TABLE IF NOT EXISTS dbtest_tableSix(
  id INT NOT NULL ,
  title VARCHAR(100),
  director VARCHAR(50),
  rate DOUBLE,
  checked_out_by VARCHAR(100),
  PRIMARY KEY (id),
  FOREIGN KEY (checked_out_by) REFERENCES dbtest_tablefour(customer_name)


);

INSERT INTO dbtest_tableSix VALUES
  (10001, 'Pulp Fiction', 'Quentin Tarantino', 6.00, 'Peter Jackson'),
  (10002, 'The Fugitive', 'Andrew Davis', 6.00, 'Jane Campion'),
  (1003, 'Under Siege', 'Andrew Davis', 2.00, null),
  (1004, 'The Hateful VIII', 'Quentin Tarantino', 4.00, 'Jane Campion'),
  (1005, 'The Princess Bride', 'Rob Reiner', 6.00, 'Temuera Morrison');



