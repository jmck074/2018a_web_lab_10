-- Answers to Exercise 2 here
DROP TABLE IF EXISTS dbtest_tablethree;

CREATE TABLE IF NOT EXISTS dbtest_tablethree(
  username VARCHAR (32) NOT NULL,
  first_name VARCHAR(20),
  last_name VARCHAR(50),
  email VARCHAR (50)
);

INSERT INTO dbtest_tablethree VALUES
  ('programmer3', 'Pete', 'M', 'pmei050@aucklanduni.ac.nz'),
  ('programmer4', 'Peter', 'Smith', 'psmith@wodehouse.co.uk'),
  ('programmer5', 'Kevin', 'Peterson', 'programmer5@notcricket.com'),
('programmer5', 'Johnny','5','is@alive.com');

SELECT * FROM dbtest_tablethree;