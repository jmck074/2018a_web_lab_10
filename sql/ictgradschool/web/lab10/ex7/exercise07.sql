-- Answers to Exercise 7 here
-- USE jmck074ForEx4;

DROP TABLE IF EXISTS dbtest_ex7;

CREATE TABLE IF NOT EXISTS dbtest_ex7(
  comment_id INT NOT NULL AUTO_INCREMENT,
  comment TEXT,
  article_ID INT,
  PRIMARY KEY (comment_id),
  FOREIGN KEY (article_ID) REFERENCES dbtest_tablefiveEx4(id)
);

INSERT INTO dbtest_ex7 (comment, article_ID) VALUES
  ( 'This sucks', 1),
  ( 'No, you suck',1),
  ( 'First!', 2);
