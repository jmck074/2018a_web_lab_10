-- Answers to Exercise 4 here
--  CREATE DATABASE jmck074ForEx4;

-- USE jmck074ForEx4;

DROP TABLE IF EXISTS dbtest_tablefiveEx4;

CREATE TABLE IF NOT EXISTS dbtest_tablefiveEx4(
  id INT,
  title VARCHAR(200),
  author_id INT,
  content TEXT,
  PRIMARY KEY (id));

INSERT INTO dbtest_tablefiveEx4 VALUES
  (1, 'Not Really', 0004, 'The sky was the colour of a television tuned to a dead  channel, which used to mean a cloudy grey day, but these days meant bright blue sky. A little confusing to be sure'),
  (2, 'The Greatest story ever told', 0003, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor leo a diam sollicitudin tempor id eu nisl. Enim tortor at auctor urna nunc id. At lectus urna duis convallis convallis tellus id interdum velit. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi. Augue ut lectus arcu bibendum. Cursus risus at ultrices mi tempus imperdiet nulla malesuada. Nec dui nunc mattis enim ut tellus. Magna fringilla urna porttitor rhoncus. Quam lacus suspendisse faucibus interdum posuere lorem. Sit amet consectetur adipiscing elit pellentesque. Velit sed ullamcorper morbi tincidunt.
Tortor at risus viverra adipiscing at. Vel facilisis volutpat est velit egestas dui id ornare arcu. Sed nisi lacus sed viverra tellus in hac. Pellentesque diam volutpat commodo sed egestas egestas fringilla. Diam quis enim lobortis scelerisque fermentum dui faucibus. Ornare arcu odio ut sem nulla pharetra diam. Ante metus dictum at tempor commodo. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Suscipit tellus mauris a diam maecenas sed enim ut. Id nibh tortor id aliquet lectus proin nibh.'),
  (3,'Cryptonomicon', 0000, 'Two tires wail, two fly, a bamboo forest something something I forget the rest, pretty good book though Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Hendrerit dolor magna eget est lorem ipsum dolor sit.'),
  (4, 'The Living daylights', 007, 'Does it matter that the length of the author ID is different? I do not know, this was the first James Bond movie I ever saw. There is skydiving and some sort of chase with a cello. I watched it with my Dad. I think I was seven');

