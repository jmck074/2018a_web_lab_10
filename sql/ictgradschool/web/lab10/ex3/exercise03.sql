-- Answers to Exercise 3 here
DROP TABLE IF EXISTS dbtest_tablefour;

CREATE TABLE IF NOT EXISTS dbtest_tablefour(
  customer_name VARCHAR(100) NOT NULL,
  gender VARCHAR(6),
  year_born INT,
  joined VARCHAR(4),
  num_hires INT,
  PRIMARY KEY (customer_name)
);

INSERT INTO dbtest_tablefour VALUES
  ('Peter Jackson', 'male', 1961, '1997', 17000),
  ('Jane Campion', 'female', 1954, '1980', 30000),
  ('Roger Donaldson', 'male', 1945, '1980', 12000),
  ('Temuera Morrison', 'male', 1960,'1995',15500),
  ('Russell Crowe', 'male', 1964, '1990', 10000),
  ('Lucy Lawless', 'female', 1968, '1995', 5000),
  ('Michael Hurst', 'male', 1957, '2000', 15000),
  ('Andrew Niccol', 'male', 1964, '1997', 3500),
  ('Kiri Te Kanawa', 'female',1944, '1997', 500 ),
  ('Lorde', 'female', 1996, '2010', 1000);

INSERT INTO dbtest_tablefour VALUES
  ('Peter Jackson', 'male', 1961, '1997', 17000),
  ('Kiri Te Kanawa', 'female',1944, '1997', 500 );